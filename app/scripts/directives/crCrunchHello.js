'use strict';

angular.module('crunchinatorApp.directives').directive('crCrunchHello',  function() {
  return {
    restrict: 'AEC',
    scope:{
      message:'=messageAttr',
      showMessage:'&showMessageAttr'
    },
    replace: true,
    template: '<p ng-click="clearMessage()">Hello, World! {{message}} </p>',
    link: function(scope, elem, attrs) {
      
      scope.$watch('message', function(value) {
        scope.showMessage();
      });

      scope.clearMessage = function() {
        scope.message = '';
      }

      elem.bind('mouseover', function() {
        elem.css('cursor', 'pointer');
      });

    }
  }
});